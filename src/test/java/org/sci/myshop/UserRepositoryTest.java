package org.sci.myshop;

import org.junit.jupiter.api.Test;
import org.sci.myshop.model.Role;
import org.sci.myshop.model.User;
import org.sci.myshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TestEntityManager testEntityManager;

    @Test
    public void test_createUser() {
        Role role = new Role("ADMIN");
        testEntityManager.persist(role);
        User user = new User("username1", "password", "Cosmin Popescu",
                "address", "cosmin@gmail.com", role);
        testEntityManager.persist(user);
        assertNotNull(userRepository);
    }

    @Test
    public void test_findUserById() {
        Role role1 = new Role("ADMIN");
        testEntityManager.persist(role1);
        User user1 = new User("username1", "password", "Cosmin Popescu",
                "address", "cosmin@gmail.com", role1);
        testEntityManager.persist(user1);

        Role role2 = new Role("USER");
        testEntityManager.persist(role2);
        User user2 = new User("username2", "password2", "Cosmin Pop",
                "address2", "cosmin2@gmail.com", role2);
        testEntityManager.persist(user2);

        User foundUser1 = userRepository.findUserById(user1.getId());
        assert foundUser1.equals(user1);

        User foundUser2 = userRepository.findUserById(user2.getId());
        assert foundUser2.equals(user2);
    }

    @Test
    public void test_findUsername() {
        Role role1 = new Role("ADMIN");
        testEntityManager.persist(role1);
        User user1 = new User("username1", "password", "Cosmin Popescu",
                "address", "cosmin@gmail.com", role1);
        testEntityManager.persist(user1);

        Role role2 = new Role("USER");
        testEntityManager.persist(role2);
        User user2 = new User("username2", "password2", "Cosmin Pop",
                "address2", "cosmin2@gmail.com", role2);
        testEntityManager.persist(user2);

        User foundUser1 = userRepository.findByUsername(user1.getUsername());
        assert foundUser1.equals(user1);

        User foundUser2 = userRepository.findByUsername(user2.getUsername());
        assert foundUser2.equals(user2);
    }
}
