package org.sci.myshop;

import org.junit.jupiter.api.Test;
import org.sci.myshop.model.Product;
import org.sci.myshop.model.Role;
import org.sci.myshop.model.ShoppingCart;
import org.sci.myshop.model.User;
import org.sci.myshop.repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ShoppingCartRepositoryTest {

    @Autowired
    ShoppingCartRepository shoppingCartRepository;
    @Autowired
    TestEntityManager testEntityManager;

    @Test
    public void test_findCartByUser() {
        Role role1 = new Role("ADMIN");
        testEntityManager.persist(role1);
        User user1 = new User("username1", "password", "Cosmin Popescu",
                "address", "cosmin@gmail.com", role1);
        testEntityManager.persist(user1);

        Product product = new Product("name1", "category1",
                "description", "dell", 200, "image1");
        testEntityManager.persist(product);

        ShoppingCart shoppingCart = new ShoppingCart(user1.getUsername(), product.getProductId(), "name1", "category1",
                "description", "dell", "image1", 200);
        testEntityManager.persist(shoppingCart);
        //when
        boolean expected = shoppingCartRepository.search(user1.getUsername()).get(0).getBelongsToUser().equals("username1");
        //then
        assertThat(expected).isTrue();
    }
}