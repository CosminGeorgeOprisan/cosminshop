package org.sci.myshop;

import org.junit.jupiter.api.Test;
import org.sci.myshop.model.Product;
import org.sci.myshop.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ProductRepositoryTest {

    @Autowired
    ProductsRepository productRepository;
    @Autowired
    TestEntityManager testEntityManager;


    @Test
    public void test_createProduct() {

        Product product = new Product("name1", "category1",
                "description", "dell", 200, "image1");
        testEntityManager.persist(product);
        assertNotNull(productRepository);
    }

    @Test
    public void test_findProductByName() {

        Product product = new Product("name1", "category1",
                "description", "dell", 200, "image1");
        testEntityManager.persist(product);
        //when
        boolean expected = productRepository.search("name1").get(0).getName().equals("name1");
        //then
        assertThat(expected).isTrue();
    }

    @Test
    public void test_findProductCategory() {

        Product product = new Product("name1", "category1",
                "description", "dell", 200, "image1");
        testEntityManager.persist(product);
        //when
        boolean expected = productRepository.showProductsByCategory("category1").get(0).getCategory().equals("category1");
        //then
        assertThat(expected).isTrue();
    }

    @Test
    public void test_findProductDescription() {

        Product product = new Product("name1", "category1",
                "description", "dell", 200, "image1");
        testEntityManager.persist(product);
        //when
        boolean expected = productRepository.search("description").get(0).getDescription().equals("description");
        //then
        assertThat(expected).isTrue();
    }

    @Test
    public void test_findProductManufacturer() {

        Product product = new Product("name1", "category1",
                "description", "dell", 200, "image1");
        testEntityManager.persist(product);
        //when
        boolean expected = productRepository.search("dell").get(0).getManufacturer().equals("dell");
        //then
        assertThat(expected).isTrue();
    }


    @Test
    public void test_findProductById() {

        Product product1 = new Product("name1", "category1",
                "description", "dell", 200, "image1");

        Product product2 = new Product("name2", "category2",
                "description", "huawei", 300, "image2");

        testEntityManager.persist(product1);
        testEntityManager.persist(product2);


        Product foundProduct1 = productRepository.findByProductId(product1.getProductId());
        assert foundProduct1.equals(product1);

        Product foundProduct2 = productRepository.findByProductId(product2.getProductId());
        assert foundProduct2.equals(product2);
    }
}

