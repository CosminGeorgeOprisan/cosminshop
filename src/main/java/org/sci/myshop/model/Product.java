package org.sci.myshop.model;

import javax.persistence.*;

@Entity
@Table(name = "Products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long productId;

    private String name;

    private String category;

    private String description;

    private String manufacturer;

    private double price;

    private String pictureLocation;

    public Product() {
    }

    public Product(String name, String category, String description, String manufacturer, double price, String pictureLocation) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.manufacturer = manufacturer;
        this.price = price;
        this.pictureLocation = pictureLocation;
    }

    public long getProductId() {
      return productId;
   }

   public void setProductId(long productId) {
      this.productId = productId;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getCategory() {
      return category;
   }

   public void setCategory(String category) {
      this.category = category;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getManufacturer() {
      return manufacturer;
   }

    public String getPictureLocation() {
        return pictureLocation;
    }

    public void setPictureLocation(String pictureLocation) {
        this.pictureLocation = pictureLocation;
    }

    public void setManufacturer(String manufacturer) {
      this.manufacturer = manufacturer;
   }

   public double getPrice() {
      return price;
   }

   public void setPrice(double price) {
       this.price = price;
   }
}
