package org.sci.myshop.services.interfaces;

import org.sci.myshop.model.Role;

import java.util.List;

public interface RolesService {
    void saveRoles(List<Role> roles);
    List<Role> findAllRoles();
}
