package org.sci.myshop.services.interfaces;

import org.sci.myshop.model.User;

import java.util.List;

public interface UserService {
    void save(User user);

    User findUserById(Long id);

    User findByUsername(String username);

    List<User> findAllUsers();
}
