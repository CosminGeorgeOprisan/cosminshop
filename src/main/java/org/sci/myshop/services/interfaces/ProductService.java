package org.sci.myshop.services.interfaces;

import org.sci.myshop.model.Product;

import java.util.List;

public interface ProductService {

    void save(Product product);

    List<Product> findByName(String name);

    Product findByProductId(Long id);

    List<Product> findAllProducts();

    void findAndAddToCartById(Long id, String username);

    void deleteProductById(long id);
}
