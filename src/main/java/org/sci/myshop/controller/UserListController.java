package org.sci.myshop.controller;

import org.sci.myshop.model.User;
import org.sci.myshop.repositories.UserRepository;
import org.sci.myshop.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserListController {
    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/UsersManagement")
    public String getUserList(Model model){
        List<User> list = userService.findAllUsers();
        model.addAttribute("listOfUsers",list);

        return "UsersManagement";
    }

    @PostMapping("/UsersManagement/{id}")
    public String deleteUserById(@PathVariable Long id){
       userService.deleteUser(id);
       return "redirect:/UsersManagement";
    }

    @GetMapping("/showUpdateForm")
    public String showUpdateForm(@RequestParam Long userId, Model model) {
        User user = userService.findUserById(userId);
        model.addAttribute("user", user);
        return "EditRole";
    }

    @PostMapping("/UpdateUser")
    public String UpdateUser(Model model, User user) {
        User userInDb = userService.findUserById(user.getId());
        userInDb.setRole(user.getRole());
        model.addAttribute("user", userInDb);
        userService.save(userInDb);
        return "redirect:/UsersManagement";
    }
}
