package org.sci.myshop.controller;


import org.sci.myshop.model.*;
import org.sci.myshop.services.ProductServiceImpl;
import org.sci.myshop.services.ShoppingCartServiceImpl;
import org.sci.myshop.services.UserServiceImpl;
import org.sci.myshop.services.interfaces.RolesService;
import org.sci.myshop.services.interfaces.SecurityService;
import org.sci.myshop.utils.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private RolesService rolesService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private ShoppingCartServiceImpl shoppingCartContentService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {

        userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "Welcome";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/Welcome";
    }

    @GetMapping("/login")
    public String login(Model model, String error) {
        if (error != null)
            model.addAttribute("error", "Your username or password is invalid.");

            return "Welcome";
    }

    @GetMapping({"/", "/Welcome"})
    public String getWelcomePage(Model model) {
        model.addAttribute("userForm", new User());

        if (servletContext.getAttribute("init")==null) {
            initUsersData();
            initProductsData();
            servletContext.setAttribute("init", true);
        }
            return "Welcome";
    }

    @GetMapping("/EditProfile")
    public String editProfile(Model model, @AuthenticationPrincipal UserDetails currentUser){
    User loggedUser = userService.findByUsername(currentUser.getUsername());
    model.addAttribute("loggedUser", loggedUser);

    User newUser = new User();
    model.addAttribute("newUser", newUser);

    return "EditProfile";
    }

    @PostMapping("/SaveChanges")
    public String editUser(@ModelAttribute("newUser") User newUser, @AuthenticationPrincipal UserDetails currentUser, Model model, HttpSession httpSession){
        User loggedUser = userService.findByUsername(currentUser.getUsername());
        model.addAttribute("loggedUser", loggedUser);

        if (!loggedUser.getUsername().equals(newUser.getUsername())) {

            newUser.setRole(loggedUser.getRole());

            if(newUser.getUsername() == null ||  newUser.getUsername().length() < 5) {
               newUser.setUsername(loggedUser.getUsername());
            }
            else{
                loggedUser.setUsername(newUser.getUsername());
            }

            if(newUser.getFullName() != null && newUser.getFullName().length() >3 ) {
                loggedUser.setFullName(newUser.getFullName());
            }
            else{
                newUser.setFullName(loggedUser.getFullName());
            }

            if(newUser.getAddress() != null && newUser.getAddress().length() >3 ) {
                loggedUser.setAddress(newUser.getAddress());
            }
            else{
                newUser.setAddress(loggedUser.getAddress());
            }

            if(newUser.getEmail() != null && newUser.getEmail().length() >3 ) {
                loggedUser.setEmail(newUser.getEmail());
            }
            else{
                newUser.setEmail(loggedUser.getEmail());
            }

            if(newUser.getPassword() != null && newUser.getPassword().length() > 7){
                loggedUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));
            }
            else{
                newUser.setPassword(loggedUser.getPassword());
            }

            if(newUser.getPasswordConfirm() != null && newUser.getPasswordConfirm().equals(newUser.getPassword())){
                loggedUser.setPasswordConfirm(bCryptPasswordEncoder.encode(newUser.getPasswordConfirm()));
            }
            else {
                newUser.setPasswordConfirm(loggedUser.getPasswordConfirm());
            }

            userService.updateUser(loggedUser, newUser);
            httpSession.invalidate();

        }
        return "redirect:/welcome";
    }

    private void initUsersData(){
        List<Role> roles = new ArrayList<>();
        Role adminRole =new Role();
        Role userRole =new Role();

        adminRole.setName("ADMIN");
        userRole.setName("USER");
        roles.add(adminRole);
        roles.add(userRole);

        rolesService.saveRoles(roles);
        roles=rolesService.findAllRoles();

        String[] adminUsers = {"Cosmin", "Bogdan", "Daniel"};
        for (int i = 0;i<adminUsers.length;i++){
            User admins = new User();
            admins.setUsername(adminUsers[i]);
            admins.setPassword("password");
            admins.setFullName(adminUsers[i] + " Popescu");
            admins.setEmail(adminUsers[i] + "@gmail.com");
            admins.setAddress(adminUsers[i] + "'s " + "address");
            admins.setRole(roles.get(0));

            userService.save(admins);
        }

        String[] customers = {"Alexandru", "Andreea", "Andrei"};
        for (int i = 0;i < customers.length;i++){
            User customer = new User();
            customer.setUsername(customers[i]);
            customer.setPassword("password");
            customer.setFullName(customers[i] + " Popa");
            customer.setEmail(customers[i] + "@gmail.com");
            customer.setAddress(customers[i] + " address");
            customer.setRole(roles.get(1));
            userService.save(customer);

        }
    }

    private void initProductsData(){

        String[] laptopsProductNames = {"ALLVIEW AllBook H", "LENOVO Legion 5", "ASUS X415EA", "HP 15s", "ACER Aspire 5", "HUAWEI MateBook 14", "APPLE MacBook Air 13"};
        String[] laptopsImageLocations= {"/images/laptops/0.jpg", "/images/laptops/1.jpg", "/images/laptops/2.jpg", "/images/laptops/3.jpg", "/images/laptops/4.jpg", "/images/laptops/5.jpg", "/images/laptops/6.jpg"};
        String[] laptopManufacturers = {"ALLVIEW", "LENOVO", "ASSUS", "HP", "ACER", "HUAWEI", "APPLE"};
        for(int i = 0; i<laptopManufacturers.length; i++){
            Product laptop = new Product();
            laptop.setName(laptopsProductNames[i]);
            laptop.setCategory("laptops");
            laptop.setPictureLocation(laptopsImageLocations[i]);
            laptop.setDescription("Very good");
            laptop.setManufacturer(laptopManufacturers[i]);
            laptop.setPrice(500.25 + i);

            productService.save(laptop);

        }

        String[] monitorsProductNames = {"Monitor Gaming LG", "Monitor DELL", "Monitor SAMSUNG", "Monitor ASUS", "Monitor HP", "Monitor HUAWEI"};
        String[] monitorsImageLocations = {"/images/monitors/0.jpg", "/images/monitors/1.jpg", "/images/monitors/2.jpg", "/images/monitors/3.jpg", "/images/monitors/4.jpg", "/images/monitors/5.jpg"};
        String[] monitorsManufacturers= {"LG", "DELL", "SAMSUNG", "ASUS", "HP", "HUAWEI"};

        for(int j = 0; j<monitorsProductNames.length; j++){
            Product monitors = new Product();
            monitors.setName(monitorsProductNames[j]);
            monitors.setCategory("monitors");
            monitors.setPictureLocation(monitorsImageLocations[j]);
            monitors.setDescription("Very good");
            monitors.setManufacturer(monitorsManufacturers[j]);
            monitors.setPrice(200.30 + j);

            productService.save(monitors);
        }

        String[] peripheralProductNames = {"Tastatura gaming MYRIA", "Mouse gaming LOGITECH", "Boxe HAMA Sonic",  "Camera Web PROMATE ProCam-2", "Casti Gaming RAZER Kraken X Lite", "Memorie USB ADDLINK U55", "Hard Disk extern SEAGATE Expansion"};
        String[] peripheralImageLocations = {"/images/peripheral/0.jpg", "/images/peripheral/1.jpg", "/images/peripheral/2.jpg", "/images/peripheral/3.jpg", "/images/peripheral/4.jpg", "/images/peripheral/5.jpg", "/images/peripheral/6.jpg" };
        String[] peripheralProductManufacturers = {"MYRIA", "LOGITECH", "HAMA", "PROMATE", "RAZER", "ADDLINK", "SEAGATE"};

        for(int k = 0; k< peripheralProductNames.length; k++){
            Product peripheral = new Product();
            peripheral.setName(peripheralProductNames[k]);
            peripheral.setCategory("peripheral");
            peripheral.setPictureLocation(peripheralImageLocations[k]);
            peripheral.setDescription("Very good");
            peripheral.setManufacturer(peripheralProductManufacturers[k]);
            peripheral.setPrice(15.25 + k);

            productService.save(peripheral);
        }

        String[] phonesProductNames = {"Telefon SAMSUNG Galaxy A22", "Telefon APPLE iPhone 12", "Telefon OPPO A16", "Telefon MOTOROLA Moto E7", "Telefon HUAWEI nova 9 SE", "Telefon ALLVIEW A20 Max", "Telefon XIAOMI Redmi 10C"};
        String[] phonesImageLocation = {"/images/phones/0.jpg", "/images/phones/1.jpg", "/images/phones/2.jpg", "/images/phones/3.jpg", "/images/phones/4.jpg", "/images/phones/5.jpg", "/images/phones/6.jpg"};
        String[] phonesProductManufacturers = {"SAMSUNG", "APPLE", "OPPO", "MOTOROLA", "HUAWEI", "ALLVIEW", "XIAOMI"};

        for(int l = 0; l<phonesProductNames.length; l++){
            Product phones = new Product();
            phones.setName(phonesProductNames[l]);
            phones.setCategory("phones");
            phones.setPictureLocation(phonesImageLocation[l]);
            phones.setDescription("Very good");
            phones.setManufacturer(phonesProductManufacturers[l]);
            phones.setPrice(80.30 + l);

            productService.save(phones);
        }

        String[] printersProductName = {"Imprimanta CANON PIXMA", "Imprimanta XEROX Phaser", "Imprimanta LEXMARK B22", "Imprimanta BROTHER", "Imprimanta HP", "Imprimanta EPSON ITS"};
        String[] printersImageLocation = {"/images/printers/0.jpg", "/images/printers/1.jpg", "/images/printers/2.jpg", "/images/printers/3.jpg", "/images/printers/4.jpg", "/images/printers/5.jpg"};
        String[] printersManufacturers = {"CANON", "XEROX", "LEXMARK", "BROTHER", "HP", "EPSON"};

        for (int m = 0; m<printersProductName.length; m++){
            Product printers = new Product();
            printers.setName(printersProductName[m]);
            printers.setCategory("printers");
            printers.setPictureLocation(printersImageLocation[m]);
            printers.setDescription("Very good");
            printers.setManufacturer(printersManufacturers[m]);
            printers.setPrice(35.67 + m);

            productService.save(printers);

        }

        String[] tabletsProductName = {"Tableta LENOVO Tab M10", "Tableta APPLE iPad9", "Tableta SAMSUNG Galaxy Tab S8", "Tableta HUAWEI MatePad T10", "Tableta ALLVIEW Viva"};
        String[] tabletsImageLocation = {"/images/tablets/0.jpg", "/images/tablets/1.jpg", "/images/tablets/2.jpg", "/images/tablets/3.jpg", "/images/tablets/4.jpg"};
        String[] tabletsManufacturers = {"LENOVO", "APPLE", "SAMSUNG", "HUAWEI", "ALLVIEW"};

        for(int n = 0; n<tabletsProductName.length; n++){
            Product tablets = new Product();
            tablets.setName(tabletsProductName[n]);
            tablets.setCategory("tablets");
            tablets.setPictureLocation(tabletsImageLocation[n]);
            tablets.setDescription("Very good");
            tablets.setManufacturer(tabletsManufacturers[n]);
            tablets.setPrice(38.23 + n);

            productService.save(tablets);
        }
    }
}